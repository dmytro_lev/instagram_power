#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Use text editor to edit the script and type in valid Instagram username/password

from InstagramAPI import InstagramAPI
from pprint import pprint
from time import sleep
from random import shuffle


def getTotalFollowers(api, user_id):
    """
    Returns the list of followers of the user.
    It should be equivalent of calling api.getTotalFollowers from InstagramAPI
    """

    followers = []
    next_max_id = True
    while next_max_id:
        # first iteration hack
        if next_max_id is True:
            next_max_id = ''

        _ = api.getUserFollowers(user_id, maxid=next_max_id)
        followers.extend(api.LastJson.get('users', []))
        next_max_id = api.LastJson.get('next_max_id', '')
    return followers

def start_unfollow():

    api = InstagramAPI('','')
    api.login()

    # user_id = '1461295173'
    user_id = api.username_id

    ira = api.getTotalFollowings(user_id)
    print(ira[0])
    ira.reverse()
    print(ira[0])
    counter = 0
    print(len(ira))
    for f in ira:
        if counter <=50:
            print(f['username'])
            api.unfollow(f['pk'])
            sleep(24)
            counter+=1
    api.logout()


if __name__ == "__main__":
    MAX_USERS = 500
    counter = 0
    while counter<=MAX_USERS:
        start_unfollow()
        counter+=50
        sleep(60)

